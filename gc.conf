; gt-checksum 配置文件参考

; 定义源、目标数据源
; 目前只支持MySQL、Oracle两种数据源

[DSNs]
;oracle的连接串格式为：oracle|user/password@ip:port/sid
;例如：srcDSN = oracle|pcms/abc123@172.16.0.1:1521/helowin

;mysql的连接串格式为：mysql|usr:password@tcp(ip:port)/dbname?charset=xxx
srcDSN = mysql|u1:p1@tcp(172.17.0.1:3307)/information_schema?charset=utf8mb4
dstDSN = mysql|u1:p1@tcp(172.17.0.2:3307)/information_schema?charset=utf8mb4

; 定义校验数据对象
[Schema]
; 选项tables用来定义校验数据表对象，支持通配符"%"和"*"
; 例如：
; *.* 表示所有库表对象（MySQL不包含 information_schema\mysql\performance_schema\sys）
; test.* 表示test库下的所有表
; test.t% 表示test库下所有表名中包含字母"t"开头的表
; db%.* 表示所有库名中包含字母"db"开头的数据库中的所有表
; %db.* 表示所有库名中包含字母"db"结尾的数据库中的所有表
;
; 如果已经设置为 "*.*"，则不能再增加其他的规则，例如：设置 "*.*,pcms%.*" 则会报告规则错误
; 如果 table 和 ignore-tables 设置的值相同的话也会报告规则错误

tables = db1.t1
ignore-tables =

; 设置是否检查没有索引的表，可设置为：yes/no，默认值为：no
checkNoIndexTable = yes

; 设置是否忽略表名大小写，统一使用小写表名，可设置为：yes/no，默认值为：no
lowerCaseTableNames = no

; 设置日志文件名及等级
[Logs]
; 设置日志文件名，可以指定为绝对路径或相对路径
log = ./gt-checksum.log

; 设置日志等级，支持 debug/info/warn/error 几个等级，默认值为：info
logLevel = info

; 其他校验规则
[Rules]
; 数据校验并行线程数
parallel-thds = 10

; 设置单列索引每次检索多少条数据进行校验，默认值：10000
singleIndexChanRowCount = 10000

; 设置多列索引每次检索多少条数据进行校验
jointIndexChanRowCount = 10000

; 设置校验队列深度，默认值：100
queue-size = 100

; 设置数据校验模式，支持 count/rows/sample 三种模式，默认值为：rows
; count 表示只校验源、目标表的数据量
; rows 表示逐行校验源、目标数据
; sample 表示只进行抽样数据校验，配合参数ratio设置采样率
checkMode = rows

; 当 checkMode = sample 时，设置数据采样率，设置范围1-100，用百分比表示，1表示1%，100表示100%，默认值：10
ratio = 10

; 设置数据校验对象，支持 data/struct/index/partitions/foreign/trigger/func/proc，默认值为：data
; 分别表示：行数据/表结构/索引/分区/外键/触发器/存储函数/存储过程
checkObject = data

; 设置数据修复方案
[Repair]
; 数据修复方式，支持 file/table 两种方式
; file，生成数据修复SQL文件
; table 直接在线修复数据
datafix = file

; 修复事务数，即单个事务包含多少个dml语句，默认值为：100
fixTrxNum = 100

; 当 datafix = file 时，设置生成的SQL文件名，可以指定为绝对路径或相对路径
; 当 datafix = table 时，可以不用设置 fixFileName 参数
fixFileName = ./gt-checksum-DataFix.sql
